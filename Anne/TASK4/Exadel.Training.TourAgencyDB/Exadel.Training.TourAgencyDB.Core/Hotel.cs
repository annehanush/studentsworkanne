﻿using System;

namespace Exadel.Training.TourAgencyDB.Core
{
    public class Hotel
    {
        public string Name { get; set; }
        public int Rating { get; set; }
        public string TypeOfDiet { get; set; }
        public string Descriprion { get; set; }
        public int ResortId { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Name = parts[0];
            Rating = Int32.Parse(parts[1]);
            TypeOfDiet = parts[2];
            Descriprion = parts[3];
            ResortId = Int32.Parse(parts[4]);
        }
    }
}
