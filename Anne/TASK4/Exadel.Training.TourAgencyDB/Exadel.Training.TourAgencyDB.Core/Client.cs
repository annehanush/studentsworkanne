﻿namespace Exadel.Training.TourAgencyDB.Core
{
    public class Client
    {
        public string Name { get; set; }
        public string Passport { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }

        public void piece(string line)
        {
            string[] parts = line.Split(',');
            Name = parts[0];
            Passport = parts[1];
            Address = parts[2];
            Phone = parts[3];
            Password = parts[4];
        }
    }
}
