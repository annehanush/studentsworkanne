﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exadel.Training.TourAgencyDB.Forms
{
    public partial class MakingOfAnOrderForm : Form
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourAgencyConnectionString"].ConnectionString;
        int tourId, employeeId;
        string who, form;

        private void button1_Click(object sender, EventArgs e)
        {
            int clientId = -1;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("ClientToOrder", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@ClientName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = ClientTextBox.Text;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@out";
                param.SqlDbType = SqlDbType.Int;
                param.Direction = ParameterDirection.Output;
                sqlCmd.Parameters.Add(param);

                sqlCmd.ExecuteNonQuery();

                clientId = ((int)sqlCmd.Parameters["@out"].Value);

                connection.Close();
            }

            bool insurance = false;
            if (InsuranceCheckBox.Checked == true)
            {
                insurance = true;
            }
            else
            {
                insurance = false;
            }

            if (form == "Main")
            {
                tourId = Int32.Parse(TourIDTextBox.Text);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("NewOrderCreation", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Insurance";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Value = insurance;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@RegistrationDate";
                    param.SqlDbType = SqlDbType.Date;
                    param.Value = DateTime.Now.Date;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@ClientId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = clientId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@TourId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = tourId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@EmployeeId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = employeeId;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();

                    MessageBox.Show("Order was successfully added.");

                    this.Close();
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("NewOrderCreation", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Insurance";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Value = insurance;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@RegistrationDate";
                    param.SqlDbType = SqlDbType.Date;
                    param.Value = DateTime.Now.Date;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@ClientId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = clientId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@TourId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = tourId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@EmployeeId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = employeeId;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();

                    MessageBox.Show("Order was successfully added.");

                    this.Close();
                }
            }
        }

        private void MakingOfAnOrderForm_Load(object sender, EventArgs e)
        {
            if (form == "Main")
            {
                this.Height = 239;
                label2.Visible = true;
                TourIDTextBox.Visible = true;
            }
        }

        public MakingOfAnOrderForm(int idTour, int idperson, string belongTo, string fromForm)
        {
            InitializeComponent();

            tourId = idTour;
            employeeId = idperson;
            who = belongTo;
            form = fromForm;
        }

        // ClientToOrder
        // NewOrderCreation
    }
}
