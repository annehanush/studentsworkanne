﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exadel.Training.TourAgencyDB.Forms
{
    public partial class AvailableToursForm : Form
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourAgencyConnectionString"].ConnectionString;
        string country, transport, hotel, who;

        private void AvailableToursForm_Load(object sender, EventArgs e)
        {
            WorkWithAvailableTours();
            if (who == "Guest")
            {
                OrderCreation.Visible = false;
            }
        }

        private void ToursDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in ToursDataGridView.SelectedRows)
            {
                tourId = Int32.Parse(row.Cells[8].Value.ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OrderCreation_Click(object sender, EventArgs e)
        {
            // NewOrderCreation

            if (who == "Client")
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("NewOrderCreation", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Insurance";
                    param.SqlDbType = SqlDbType.Bit;
                    param.Value = true;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@RegistrationDate";
                    param.SqlDbType = SqlDbType.Date;
                    param.Value = DateTime.Now.Date;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@ClientId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = personId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@TourId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = tourId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@EmployeeId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = 4;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    connection.Close();

                    MessageBox.Show("Your order was successfully added.");

                    this.Close();
                }
            }
            else if (who == "Employee")
            {
                this.Hide();
                MakingOfAnOrderForm toShow = new MakingOfAnOrderForm(tourId, personId, who, "Tour");
                toShow.Show();
            }
            else
            {
                MessageBox.Show("I just do not understand, how did you get here...");
            }

        }

        int persons, days, tourId, personId;

        public AvailableToursForm(string countryName, string transportName, int numberOfPersons, int numberOfDays, string hotelName, int id, string belongTo)
        {
            InitializeComponent();

            country = countryName;
            transport = transportName;
            persons = numberOfPersons;
            days = numberOfDays;
            hotel = hotelName;
            personId = id;
            who = belongTo;
        }

        // SuitableTours
        private void WorkWithAvailableTours()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                var sqlCmd = new SqlCommand("SuitableTours", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@hotel";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = hotel;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@persons";
                param.SqlDbType = SqlDbType.Int;
                param.Value = persons;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@days";
                param.SqlDbType = SqlDbType.Int;
                param.Value = days;
                sqlCmd.Parameters.Add(param);

                param = new SqlParameter();
                param.ParameterName = "@transport";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 50;
                param.Value = transport;
                sqlCmd.Parameters.Add(param);

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            string tourType = dr.GetString(0);
                            DateTime departure = dr.GetDateTime(1);
                            decimal cost = dr.GetDecimal(2);
                            string hotelName = dr.GetString(3);
                            string resortName = dr.GetString(4);
                            int personsNumber = dr.GetInt32(5);
                            int daysNumber = dr.GetInt32(6);
                            string transportName = dr.GetString(7);
                            int id = dr.GetInt32(8);

                            ToursDataGridView.Rows.Add(tourType, departure.ToString("dd/MM/yyyy"), cost, hotelName, resortName, personsNumber, daysNumber, transportName, id);
                        }
                    }
                }
            }
        }

    }
}
