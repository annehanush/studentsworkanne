﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Exadel.Training.TourAgencyDB.Forms
{
    public partial class MainForm : Form
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourAgencyConnectionString"].ConnectionString;

        int personId;
        string who;

        public MainForm(int id, string belongTo)
        {
            InitializeComponent();
            personId = id;
            who = belongTo;
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            if (who == "Client")
            {
                orderMakingToolStripMenuItem.Visible = false;
                this.Width = 416;

                // ClientData

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("ClientData", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@personId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = personId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@PassportNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@PhoneNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    NameTextBox.Text = ((string)sqlCmd.Parameters["@Name"].Value).Trim();
                    PassportOrPostTextBox.Text = ((string)sqlCmd.Parameters["@PassportNumber"].Value).Trim();
                    PhoneTextBox.Text = ((string)sqlCmd.Parameters["@PhoneNumber"].Value).Trim();

                    connection.Close();
                }

                // ClientOrdersAmount

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("ClientOrdersAmount", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@personId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = personId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@amount";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    OrdersTextBox.Text = ((int)sqlCmd.Parameters["@amount"].Value).ToString();

                    connection.Close();
                }

            }
            else if (who == "Employee")
            {
                PassportOrPostLabel.Text = "Post:";

                // EmployeeData

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("EmployeeData", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@personId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = personId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Name";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@Post";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@PhoneNumber";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    NameTextBox.Text = ((string)sqlCmd.Parameters["@Name"].Value).Trim();
                    PassportOrPostTextBox.Text = ((string)sqlCmd.Parameters["@Post"].Value).Trim();
                    PhoneTextBox.Text = ((string)sqlCmd.Parameters["@PhoneNumber"].Value).Trim();

                    connection.Close();
                }

                // EmployeeOrdersAmount

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("EmployeeOrdersAmount", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@personId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = personId;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@amount";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    OrdersTextBox.Text = ((int)sqlCmd.Parameters["@amount"].Value).ToString();

                    connection.Close();
                }

            }
            else if (who == "Guest")
            {
                orderMakingToolStripMenuItem.Visible = false;

                RegisterButton.Visible = true;
                GuestInfoLabel.Visible = true;

                PersonalDataLabel.Visible = false;
                NameLabel.Visible = false;
                PassportOrPostLabel.Visible = false;
                PhoneLabel.Visible = false;
                OrdersLabel.Visible = false;
                NameTextBox.Visible = false;
                PassportOrPostTextBox.Visible = false;
                PhoneTextBox.Visible = false;
                OrdersTextBox.Visible = false;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void tourSelectionToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            TourSelectionForm toShow = new TourSelectionForm(personId, who);
            toShow.Show();
        }

        private void orderMakingToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            MakingOfAnOrderForm toShow = new MakingOfAnOrderForm(-1, personId, who, "Main");
            toShow.Show();
        }

        private void RegisterButton_Click(object sender, System.EventArgs e)
        {
            RegistrationForm toShow = new RegistrationForm();
            toShow.Show();
        }
    }
}
