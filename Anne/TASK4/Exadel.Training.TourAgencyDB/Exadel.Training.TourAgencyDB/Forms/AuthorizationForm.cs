﻿using System;
using System.Windows.Forms;
using System.Configuration;
using Exadel.Training.TourAgencyDB.Core;
using System.Data;
using System.Data.SqlClient;
using Exadel.Training.TourAgencyDB.Forms;

namespace Exadel.Training.TourAgencyDB
{
    public partial class AuthorizationForm : Form
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["TourAgencyConnectionString"].ConnectionString;

        bool emptyTables = false;

        public AuthorizationForm()
        {
            InitializeComponent();
        }

        private void AuthorizationForm_Load(object sender, EventArgs e)
        {
            PasswordTextBox.PasswordChar = '*';
            //emptyTables = true;
            if (emptyTables)
            {
                FillTablesFromFiles.FillTables();
            }
            /*else
            {
                MessageBox.Show("Tables have already been filled with the info from CSV files.");
            }*/
            
        }

        private void RegistrationButton_Click(object sender, EventArgs e)
        {
            LoginButton.Visible = false;
            JustLookButton.Visible = false;
            RegistrationButton.Visible = false;
            label7.Visible = true;

            label1.Visible = true;
            label2.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            label5.Visible = true;

            PassportTextBox.Visible = true;
            AddressTextBox.Visible = true;
            PhoneTextBox.Visible = true;
            NameTextBox.Visible = true;
            PasswordTextBox.Visible = true;

            RegistrationSubmitButton.Visible = true;

        }

        private void EmployeeButton_Click(object sender, EventArgs e)
        {
            NameTextBox.Visible = true;
            PasswordTextBox.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
            EmployeeSubmitButton.Visible = true;
            ClientButton.Enabled = false;
            RegistrationButton.Enabled = false;
        }

        private void ClientButton_Click(object sender, EventArgs e)
        {
            NameTextBox.Visible = true;
            PasswordTextBox.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
            ClientSubmitButton.Visible = true;
            EmployeeButton.Enabled = false;
            RegistrationButton.Enabled = false;
        }

        private void RegistrationSubmitButton_Click(object sender, EventArgs e)
        {
            if (NameTextBox.Text == "" || PassportTextBox.Text == "" || AddressTextBox.Text == "" || PhoneTextBox.Text == "" || PasswordTextBox.Text == "")
            {
                MessageBox.Show("Fill all the fileds, please.");
            }
            else
            {
                FillTablesFromFiles.ClientRegistration(NameTextBox.Text, PassportTextBox.Text, AddressTextBox.Text, PhoneTextBox.Text, PasswordTextBox.Text);
                Application.Restart();
            }
        }

        private void ClientSubmitButton_Click(object sender, EventArgs e)
        {
            string enteredName = NameTextBox.Text;
            string enteredPassword = PasswordTextBox.Text;
            int id = -1;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("CheckClientExistence", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@enteredName";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = enteredName;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@enteredPassword";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = enteredPassword;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@clientId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    id = ((int)sqlCmd.Parameters["@clientId"].Value);

                    connection.Close();

                    this.Hide();
                    MainForm toShow = new MainForm(id, "Client");
                    toShow.Show();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("You entered wrong data...");
            }

            

        }

        private void EmployeeSubmitButton_Click(object sender, EventArgs e)
        {
            string enteredName = NameTextBox.Text;
            string enteredPassword = PasswordTextBox.Text;
            int id = -1;

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    var sqlCmd = new SqlCommand("CheckEmployeeExistence", connection);
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@enteredName";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = enteredName;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@enteredPassword";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Size = 50;
                    param.Value = enteredPassword;
                    sqlCmd.Parameters.Add(param);

                    param = new SqlParameter();
                    param.ParameterName = "@employeeId";
                    param.SqlDbType = SqlDbType.Int;
                    param.Direction = ParameterDirection.Output;
                    sqlCmd.Parameters.Add(param);

                    sqlCmd.ExecuteNonQuery();

                    id = ((int)sqlCmd.Parameters["@employeeId"].Value);

                    connection.Close();

                    this.Hide();
                    MainForm toShow = new MainForm(id, "Employee");
                    toShow.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("You entered wrong data...");
            }

        }

        private void RestartButton_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            EmployeeButton.Visible = true;
            ClientButton.Visible = true;
            RegistrationButton.Visible = false;
            LoginButton.Visible = false;
            JustLookButton.Visible = false;
        }

        private void JustLookButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            MainForm toShow = new MainForm(-1, "Guest");
            toShow.Show();
        }
    }
}
