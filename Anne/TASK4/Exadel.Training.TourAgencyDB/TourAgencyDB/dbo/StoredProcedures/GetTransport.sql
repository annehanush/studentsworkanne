﻿CREATE PROCEDURE [dbo].[GetTransport]
	@checkedHotel NVarChar(50)
AS
	Select DISTINCT Name From Transport Where Id IN ( Select TransportId FROM Tour Where HotelId IN ( Select Id FROM Hotel Where Name = @checkedHotel ) )
