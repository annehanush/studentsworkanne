﻿CREATE PROCEDURE [dbo].[EmployeeData]
	@personId int,
	@Name NVarChar(50) output,
	@Post NVarChar(50) output,
	@PhoneNumber NVarChar(50) output
AS
	SELECT @Name = Name, @Post = Post, @PhoneNumber = PhoneNumber FROM Employee WHERE Id = @personId
