﻿CREATE PROCEDURE [dbo].[EmployeeOrdersAmount]
	@personId int,
	@amount int output
AS
	SELECT @amount = Count(*) FROM [dbo].[Order] WHERE EmployeeId = @personId
