﻿CREATE PROCEDURE [dbo].[GetNumberOfDays]
	@checkedResort NVarChar(50),
	@checkedHotel NVarChar(50)
AS
	Select DISTINCT NumberOfDays From Tour Where HotelId IN ( Select Id FROM Hotel Where  Name = @checkedHotel AND ResortId IN ( Select Id FROM Resort Where Name = @checkedResort ) )
