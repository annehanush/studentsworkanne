﻿CREATE PROCEDURE [dbo].[GetHotels]
	@checkedResort NVarChar(50),
	@selectedRating Int,
	@selectedTypeOfDiet NVarChar(50)
AS
	SELECT Name FROM Hotel WHERE ResortId IN ( SELECT Id FROM Resort WHERE Name = @checkedResort ) AND Rating = @selectedRating AND TypeOfDiet = @selectedTypeOfDiet
