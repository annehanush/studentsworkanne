﻿CREATE PROCEDURE [dbo].[ClientOrdersAmount]
	@personId int,
	@amount int output
AS
	SELECT @amount = Count(*) FROM [dbo].[Order] WHERE ClientId = @personId
