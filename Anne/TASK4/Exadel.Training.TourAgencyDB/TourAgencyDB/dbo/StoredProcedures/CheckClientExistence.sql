﻿CREATE PROCEDURE [dbo].[CheckClientExistence]
	@enteredName NVarChar(50),
	@enteredPassword NVarChar(50),
	@clientId int output
AS
	SELECT @clientId = Id FROM Client WHERE Name = @enteredName AND Password = @enteredPassword
