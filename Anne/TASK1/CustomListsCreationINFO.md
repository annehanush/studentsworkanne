Задача

Реализовать связный и двусвязный списки. Должна быть реализована возможность добавления элемента в конец списка, в начало и в середину, возможность удаления любого элемента из списка (первого, последнего или из середины). Должна быть возможность узнать, содержится ли определенный элемент в списке. Также нужно реализовать методы очистки списка, его сортировки и перестановки (реверса).
Реализовать тесты для проверки правильности работы приложения.
Должна быть реализована возможность работать с приложением через консоль.

Реализация

1. Связный список

Связный список (Linked List) представляет набор связанных узлов, каждый из которых хранит собственно данные и ссылку на следующий узел.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedList.png) 

Таким образом, был определен класс узла, который собственно представляет объект в списке.

Класс Node<T> является обобщенным, т. е., может хранить данные любого типа.
Для хранения ссылки на следующий узел используется свойство Next, для хранения данных — Data.

Для добавления элементов было реализовано 3 метода:
- Add (производится добавление элемента в конец списка);
![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListAdd.png) 
 
Сложность такого алгоритма составляет O(1).

- AddFirstElement (элемент добавляется в начало списка);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListAddFirstElement.png) 

- AddAfterElement (добавление элемента после уже существующего элемента).

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListAddAfterElement.png) 


Метод удаления Remove позволяет удалять элементы из:
- начала списка

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListRemoveFirstElement.png)  

- конца

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListRemoveLastElement.png)  

- середины

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/LinkedListRemoveElementFromCenter.png)  

Алгоритм удаления элемнта представляет следующую последовательность шагов:

1.	Поиск элемента в списке (перебором всех элементов);
2.	Установка Next у предыдущего узла на следующий узел (от удаляемого).

Сложность такого алгоритма составляет O(n).

Для проверки наличия элемента в списке был реализован метод Contains.
В данном методе осуществляется перебор. Сложность алгоритма составляет O(n).


Для очистки списка был реализован метод Clear.

Перестановка списка (реверс)
В данном методе осуществляется перестановка элементов списка. Сложность алгоритма составляет O(n).

Сортировка
В данном методе осуществляется сортировка элементов списка по методу пузырька. Сложность алгоритма составляет O(n).

 
![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/BubbleSorting.png) 


2. Двусвязный список
Двусвязные списки также представляют последовательность связанных узлов, однако теперь каждый узел хранит ссылку на следующий и на предыдущий элементы.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedList.png) 

Двунаправленность списка приходится учитывать при добавлении или удалении элемента, так как кроме ссылки на следующий элемент надо устанавливать и ссылку на предыдущий. Но в то же время у нас появляется возможность обходить список как от первого к последнему элементу, так и наоборот - от последнего к первому элементу. В остальном двусвязный список ничем не отличается от односвязного списка.
Таким образом, был определен класс узла, который собственно представляет объект в списке.

Класс NodeDouble<T> является обобщенным, т. е., может хранить данные любого типа.
Для хранения ссылки на следующий узел используется свойство Next, для хранения данных — Data, для хранения ссылки на предыдущий узел используется свойство Previous.

Для добавления элементов было реализовано 3 метода:
- Add (производится добавление элемента в конец списка);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListAdd.png) 

Сложность такого алгоритма составляет O(1).

- AddFirstElement (элемент добавляется в начало списка);

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListAddFirstElement.png) 

- AddAfterElement (добавление элемента после уже существующего элемента).

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListAddElementAfter.png) 

Удаление элементов:
Для удаления элементов был реализован метод Remove.
При удалении вначале необходимо найти удаляемый элемент. Затем в общем случае надо переустановить две ссылки.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/4188047ea0514ef5c3fffa717d09cf8188381380/Anne/TASK1/Diagrams/DoubleLinkedListRemove.png) 

Если удаляются первый и последний элемент, соответственно надо переустановить переменные firstElement и lastElement.
Сложность такого алгоритма составляет O(n).

Для проверки наличия элемента в списке был реализован метод Contains.
В данном методе осуществляется перебор. Сложность алгоритма составляет O(n).


Для очистки списка был реализован метод Clear.

Перестановка списка (реверс)
В данном методе осуществляется перестановка элементов списка. Сложность алгоритма составляет O(n).

Сортировка
В данном методе осуществляется сортировка элементов списка по методу пузырька. Сложность алгоритма составляет O(n).

Консоль:
Для осуществления работы с приложением через консоль был создан класс ConsoleHelper.
Также в данном классе было реализовано пользовательское меню для удобства работы пользователя.


Общий интерфейс:
Для того, чтобы избежать проблему избыточности кода, в программе был реализован интерфейс ICustomList<T>. В данном интерфейсе определены методы, которые реализуются в классах MySimplyConnectedList и MyDoubleConnectedList. 
Также, было разработано некоторое количество тестов, которые позволили протестировать правильность работы программы. 
