﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exadel.Training.CreateAList
{
    interface ICustomList<T> : IEnumerable<T>
    {
        void Add(T data);
        void AddFirstElement(T data);
        void AddAfterElement(T aterThisElement, T data);

        bool Contains(T data);

        bool Remove(T data);

        void Clear();

        void Reverse();
        void Sort();
    }
}
