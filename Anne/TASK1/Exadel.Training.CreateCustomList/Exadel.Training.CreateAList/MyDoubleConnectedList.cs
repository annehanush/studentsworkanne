﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Exadel.Training.CreateAList
{
    /// <summary>
    /// Double connected list.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MyDoubleConnectedList<T> : ICustomList<T> where T : IComparable
    {
        /// <summary>
        /// Amount of elements in the list.
        /// </summary>
        int count;
        public int Count { get { return count; } }

        NodeDouble<T> firstElement;
        NodeDouble<T> lastElement;

        /// <summary>
        /// Add an element to the end of the list.
        /// </summary>
        /// <param name="data"></param>
        public void Add(T data)
        {
            NodeDouble<T> node = new NodeDouble<T>(data);

            if (firstElement == null)
            {
                firstElement = node;
            }
            else
            {
                lastElement.Next = node;
                node.Previous = lastElement;
            }
            lastElement = node;
            count++;
        }

        /// <summary>
        /// Add element to the begining of the list.
        /// </summary>
        /// <param name="data"></param>
        public void AddFirstElement(T data)
        {
            NodeDouble<T> node = new NodeDouble<T>(data);

            if (count == 0)
            {
                firstElement = node;
            }
            else
            {
                node.Next = firstElement;
                firstElement.Previous = node;
                firstElement = node;
            }
            count++;
        }

        /// <summary>
        /// If you want to add new element not to the end and not to the begining, but after any other element.
        /// </summary>
        /// <param name="aterThisElement"></param>
        /// <param name="data"></param>
        public void AddAfterElement(T aterThisElement, T data)
        {
            NodeDouble<T> currentElement = firstElement;
            NodeDouble<T> node = new NodeDouble<T>(data);

            while (currentElement != null)
            {
                if (currentElement.Data.Equals(aterThisElement))
                {
                    NodeDouble<T> elementAfterCurrent = currentElement.Next;
                    node.Next = elementAfterCurrent;
                    currentElement.Next = node;
                    elementAfterCurrent.Previous = node;
                    node.Previous = currentElement;
                    count++;
                }
                currentElement = currentElement.Next;
            }
        }

        /// <summary>
        /// Check out if there is a certain element in the list.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            NodeDouble<T> currentElement = firstElement;
            while (currentElement != null)
            {
                if (currentElement.Data.Equals(data))
                {
                    return true;
                }
                currentElement = currentElement.Next;
            }
            return false;
        }

        /// <summary>
        /// Remove any elemt of the list.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Remove(T data)
        {
            NodeDouble<T> currentElement = firstElement;

            while (currentElement != null)
            {
                if (currentElement.Data.Equals(data))
                {
                    // If the element, you wanted to remove is first.
                    if (currentElement.Previous == null)
                    {
                        firstElement = firstElement.Next;
                    }
                    else
                    {
                        // If the element, you wanted to remove is not first and last.
                        currentElement.Previous.Next = currentElement.Next;

                        // If the element, you wanted to remove is last.
                        if (currentElement.Next == null)
                        {
                            lastElement = currentElement.Previous;
                        }
                        else
                        {
                            currentElement.Next.Previous = currentElement.Previous;
                        }
                    }
                    count--;
                    return true;
                }
                currentElement.Previous = currentElement;
                currentElement = currentElement.Next;
            }
            return false;
        }

        /// <summary>
        /// Indexator.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public T this[int key]
        {
            get
            {
                var current = firstElement;
                for (int i = 0; i < key; i++)
                {
                    current = current.Next;
                }
                return current.Data;
            }
            set
            {
                var current = firstElement;
                for (int i = 0; i < key; i++)
                {
                    current = current.Next;
                }
                current.Data = value;
            }
        }

        /// <summary>
        /// Clear all the list.
        /// </summary>
        public void Clear()
        {
            firstElement = null;
            lastElement = null;
            count = 0;
        }

        /// <summary>
        /// Reverse of elements in the list.
        /// </summary>
        public void Reverse()
        {
            if (firstElement == null || firstElement.Next == null) return;
            while (firstElement.Next != null)
            {
                NodeDouble<T> next = firstElement.Next;
                firstElement.Next = firstElement.Previous;
                firstElement.Previous = firstElement;
                firstElement = next;
            }
            firstElement.Next = firstElement.Previous;
        }

        public void Sort()
        {
            bool flag = true;
            while (flag)
            {
                flag = false;
                for (var current = firstElement; current.Next != null; current = current.Next)
                {
                    if (current.CompareTo(current.Next) > 0) continue;
                    var tmp = current.Data;
                    current.Data = current.Next.Data;
                    current.Next.Data = tmp;
                    flag = true;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            NodeDouble<T> current = firstElement;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }
    }
}
