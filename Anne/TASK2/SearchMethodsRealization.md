Task

Create Windows Forms application that will allow user to load information from file and use two methods (binary search tree and hash table)
to find a name of a person by his phone number.
Count the speed of search. 

Realization

Certain application consists of 8 forms that represent all the activities, that were described before:

-  MainForm

-  FileContents

-  SearchUsingHashForm

-  SearchUsingBinaryTreeForm

-  HashTableInfoForm

-  BinarySearchTreeInfoForm

-  LookOnTree

-  HashTableLook




1)  Main Form



![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/MainForm.png)

Main form has such elements like labels, button and menu.
Main form menu:

- File 

    Here you can load file from system. Look through this file. You can also use simple search for the dataGridView. 
    Also you can save all the info into XML file.

- Search

    There are 2 types of search able in this program. Using this menu item you can try search using Hash table or search using binary tree.

- About

    This menu item gives you a possibility to chack some information about hash tables and binary trees.

- Exit 

    Closes program.

Main form is used for easy navigation by the program. 


2)  FileContents form



When you choose on the main form menu item "Look through", the program opens a new form for you. The name of the form is "FileContents".

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/LookThrough.png)

There are such components like: 4 button, textBox and dataGridView.

When you click on button "Show", you will see a dialog window, where you are able to choose any .csv file to open.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/OpenDialog.png)

After that you will see the file info in dataGridView.
You can use "Clear" button to Clear dataGridView and info about file.

Also you can try to use dataGridView search by clicking "Simple Search" button. It will try to find the name of a person with the number, you entered into textBox.

"Back" button Closes this form and Shows you Main form of the app.



3)  Writing XML



You can save the information, that was used for search in program to the .xml file. 
For doing this you should choose Main menu item "File" and after that "Writing XML".

If writing was done successfully, you would see the message like this:

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/WritingXML.png)



4)  SearchUsingBinaryTree



When you choose on the main form menu item "Search" and than item "Binary search" you will see the new form.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/TreeSearch.png)

It consists of labels, textBoxes and buttons.

You can enter the phone number into the left textBox and click "Find" and if there is a person with such number, you will see the name of this person in the right textBox.

If you click on "Tree" button, you will be able to see the new form.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/b982c5e6904fac3196ec6aa395a67c012d6ba2e8/Anne/TASK2/Diagrams/Scrins/TreeBuild.png)

On this form with the help of component treeView the tree was build. You can open the nodes to check the tree.



5)  SearchUsingHash



When you choose on the main form menu item "Search" and than item "Hash search" you will see the new form.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/HashSearch.png)

It consists of labels, textBoxes and buttons.

You can enter the phone number into the left textBox and click "Find" and if there is a person with such number, you will see the name of this person in the right textBox.
After the name was found you can click on the "Special" button and you will see some more info like the speed of search, occupancy and actual size of hash table.

If you click on "Table" button, you will be able to see the new form.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/HashTableBuild.png)

On this form you can see the info, that is in hash table and also new positions of elements that was generated in program.



6)  About



When you choose on the main form menu item "About", you can open 2 forms to get some more info about hash tables and binary trees.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/TreeInfo.png)

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/2f20065854e1d4e40dee80c8dae7c41c0e530cda/Anne/TASK2/Diagrams/Scrins/HashTableInfo.png)



Details



- Hash Table


Hash Table is a data structure which stores data in an associative manner. In a hash table, data is stored in an array format, where each data value has its own unique index value. Access of data becomes very fast if we know the index of the desired data.

Thus, it becomes a data structure in which insertion and search operations are very fast irrespective of the size of the data. Hash Table uses an array as a storage medium and uses hash technique to generate an index where an element is to be inserted or is to be located from.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/72d3e20c952674a274eae63673e18f7256988d53/Anne/TASK2/Diagrams/Hash%20Table.gif)

A hash table, put simply, is an abstraction of an array that allows any value to be used as an index. While an array requires that indices be integers, a hash table can use a floating-point value, a string, another array, or even a structure as the index. This index is called the key, and the contents of the array element at that index is called the value. So a hash table is a data structure that stores key/value pairs and can be quickly searched by the key. Because insertion and removal are operations dependent on the speed of the search, they tend to be fast as well.

Table size and range finding

The hash functions introduced in The Art of Hashing were designed to return a value in the full unsigned range of an integer. For a 32-bit integer, this means that the hash functions will return a value in the range [0..4,294,967,296). Because it is extremely likely that your table will be smaller than this, it is possible that the hash value may exceed the boundaries of the array.

The solution to this problem is to force the range down so that it fits the table size. A table size should not be chosen randomly because most of the collision resolution methods require that certain conditions be met for the table size or they will not work correctly. Most of the time, this required size is either a power of two, or a prime number.

A table size of a power of two may be desirable on some implementations where bitwise operations offer performance benefits. The way to force a value into the range of a power of two can be performed quickly with a masking operation.

Collision resolution

In a hash table, collisions, where two keys map to the same index, are resolved by finding another place to put the new key without affecting the old key, and still allowing for quick lookup of the new key. Probably the most obvious way to do this is to create a second table for collisions and place the key at the first empty location. This method is called resolution by overflow, where an “overflow” table is used to hold collisions.



- Binary Search Tree


Binary Search Tree, is a node-based binary tree data structure which has the following properties:
- The left subtree of a node contains only nodes with keys less than the node’s key.
- The right subtree of a node contains only nodes with keys greater than the node’s key.
- The left and right subtree each must also be a binary search tree.
- There must be no duplicate nodes.

![alt tag](https://bytebucket.org/annehanush/studentsworkanne/raw/72d3e20c952674a274eae63673e18f7256988d53/Anne/TASK2/Diagrams/BinaryTree.png)

The above properties of Binary Search Tree provide an ordering among keys so that the operations like search, minimum and maximum can be done fast. If there is no ordering, then we may have to compare every key to search a given key.

Search Operation
Whenever an element is to be searched, start searching from the root node. Then if the data is less than the key value, search for the element in the left subtree. Otherwise, search for the element in the right subtree. Follow the same algorithm for each node.

Insert Operation
Whenever an element is to be inserted, first locate its proper location. Start searching from the root node, then if the data is less than the key value, search for the empty location in the left subtree and insert the data. Otherwise, search for the empty location in the right subtree and insert the data.

Traversal

Once the binary search tree has been created, its elements can be retrieved in-order by recursively traversing the left subtree of the root node, accessing the node itself, then recursively traversing the right subtree of the node, continuing this pattern with each node in the tree as it's recursively accessed. As with all binary trees, one may conduct a pre-order traversal or a post-order traversal, but neither are likely to be useful for binary search trees. An in-order traversal of a binary search tree will always result in a sorted list of node items (numbers, strings or other comparable items).
