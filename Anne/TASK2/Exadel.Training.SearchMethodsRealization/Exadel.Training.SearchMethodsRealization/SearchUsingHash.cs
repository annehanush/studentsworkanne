﻿using System.Collections.Generic;

namespace Exadel.Training.SearchMethodsRealization
{
    public static class SearchUsingHash
    {
        /// <summary>
        /// Get full hash table.
        /// </summary>
        /// <returns></returns>
        public static HashTable<string, string> GetHashTable()
        {
            HashTable<string, string> ht = new HashTable<string, string>();

            List<CSVReader> infoList = new List<CSVReader>();
            infoList = CSVReader.ReadFile(@"..\..\DataFiles\inner.csv");

            for (int i = 0; i < infoList.Count; i++)
            {
                ht.Add(infoList[i].Phone, infoList[i].Name);
            }

            return ht;
        }

    }
}
