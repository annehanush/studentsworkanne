﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exadel.Training.SearchMethodsRealization
{
    public class BTree
    {
        private Random random;
        public BNode root;
        public int count;
        private IComparer<MyObject> comparer = Comparer<MyObject>.Default;


        public BTree()
        {
            root = null;
            count = 0;
            random = new Random(1);
        }

        /// <summary>
        /// Adding of an element. 
        /// If it is not root, we use Add_Sub
        /// </summary>
        /// <param name="Item"></param>
        /// <returns></returns>
        public bool Add(MyObject Item)
        {
            if (root == null)
            {
                root = new BNode(Item);
                count++;
                return true;
            }
            else
            {
                return Add_Sub(root, Item);
            }
        }

        /// <summary>
        /// Adding of a new element, that is not a root.
        /// </summary>
        /// <param name="Node"></param>
        /// <param name="Item"></param>
        /// <returns></returns>
        private bool Add_Sub(BNode Node, MyObject Item)
        {
            if (comparer.Compare(Node.item, Item) < 0)
            {
                if (Node.right == null)
                {
                    Node.right = new BNode(Item);
                    count++;
                    return true;
                }
                else
                {
                    return Add_Sub(Node.right, Item);
                }
            }
            else if (comparer.Compare(Node.item, Item) > 0)
            {
                if (Node.left == null)
                {
                    Node.left = new BNode(Item);
                    count++;
                    return true;
                }
                else
                {
                    return Add_Sub(Node.left, Item);
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Find name by key (phone number).
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string Find(string number)
        {
            return FindNode(number).item.Name;
        }

        /// <summary>
        /// Find node with certain key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="ExceptionIfKeyNotFound"></param>
        /// <returns></returns>
        public BNode FindNode(string key, bool ExceptionIfKeyNotFound = true)
        {
            BNode current = root;
            while (current != null)
            {
                int compare = current.item.Number.GetHashCode().CompareTo(key.GetHashCode());
                if (compare == 0)
                    return current;
                if (compare < 0)
                    current = current.right;
                else
                    current = current.left;
            }
            if (ExceptionIfKeyNotFound)
                throw new KeyNotFoundException();
            else
                return null;
        }

        /// <summary>
        /// Префиксный обход - от корня влево, потом вправо
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected BNode InOrderSuccesor(BNode node)
        {
            BNode succesor = node.right;
            while (succesor.left != null)
                succesor = succesor.left;
            return succesor;
        }

        /// <summary>
        /// Инфиксный обход - от наименьшего к большему
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        protected BNode InOrderPredecessor(BNode node)
        {
            BNode succesor = node.left;
            while (succesor.right != null)
                succesor = succesor.right;
            return succesor;
        }

        /// <summary>
        /// Remove note from binary tree.
        /// </summary>
        /// <param name="node"></param>
        protected void RemoveNode(BNode node)
        {
            if (node == null)
                throw new ArgumentNullException();
            //2 childs
            if (node.left != null && node.right != null)
            {
                BNode replaceBy = random.NextDouble() > .5 ? InOrderSuccesor(node) : InOrderPredecessor(node);
                RemoveNode(replaceBy);
                node.item.Number = replaceBy.item.Number;
                node.item.Name = replaceBy.item.Name;
                count--;
            }
            //1 or less childs 
            else
            {
                var child = node.left == null ? node.right : node.left;
                if (node.parent.right == node)
                    node.parent.right = child;
                else
                    node.parent.left = child;
                count--;
            }
        }


    }
}
