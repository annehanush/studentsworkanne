﻿using System.Collections.Generic;

namespace Exadel.Training.SearchMethodsRealization
{
    public static class SearchUsingBinaryTree
    {
        /// <summary>
        /// Get full binary search tree.
        /// </summary>
        /// <returns></returns>
        public static BinarySearchTree<string, string> GetBinaryTree()
        {
            BinarySearchTree<string, string> bst = new BinarySearchTree<string, string>();

            List<CSVReader> infoList = new List<CSVReader>();
            infoList = CSVReader.ReadFile(@"..\..\DataFiles\inner.csv");

            for (int i = 0; i < infoList.Count; i++)
            {
                bst.Insert(infoList[i].Phone, infoList[i].Name);
            }

            return bst;
        }
    }
}
