﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Exadel.Training.SearchMethodsRealization.Forms
{

    public partial class FileСontents : Form
    {
        public FileСontents()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Go back to MainForm.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackToMainForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Fill dataGridView from file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FillTableButton_Click(object sender, EventArgs e)
        {
            //Create new copy
            List<CSVReader> CSV_Struct = new List<CSVReader>();
            CSV_Struct = CSVReader.ReadFile(@"..\..\DataFiles\inner.csv");

            MainList.Rows.Add(CSV_Struct.Count);
            for (int i = 0; i < CSV_Struct.Count; i++)
            {
                MainList.Rows[i].Cells[0].Value = CSV_Struct[i].Phone;
                MainList.Rows[i].Cells[1].Value = CSV_Struct[i].Name;
            }
            SimpleSearch.Visible = true;
            SearchText.Visible = true;
        }

        /// <summary>
        /// Simple search through the data grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < MainList.RowCount; i++)
            {
                MainList.Rows[i].Selected = false;
                for (int j = 0; j < MainList.ColumnCount; j++)
                    if (MainList.Rows[i].Cells[j].Value != null)
                        if (MainList.Rows[i].Cells[j].Value.ToString().Contains(SearchText.Text))
                        {
                            MainList.Rows[i].Selected = true;
                            MainList.FirstDisplayedScrollingRowIndex = i;
                            break;
                        }
            }
        }

        /// <summary>
        /// To clear datagridView.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearPriceList_Click(object sender, EventArgs e)
        {
            MainList.Rows.Clear();
        }
    }
}
